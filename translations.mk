# Translations
PRODUCT_PACKAGES += \
    ApertureTranslation \
    GameSpaceTranslation \
	KernelSUTranslation  \
    PixelExpFrameworksTranslation \
    PixelExpSettingsTranslation \
    PixelExpSystemUITranslation
